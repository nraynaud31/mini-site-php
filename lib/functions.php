<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../pages/home.php';
	} else {
		if($_GET['page'] == "bio"){
		    include __DIR__.'/../pages/bio.php';
        }
		if($_GET['page'] == "contact"){
		    include __DIR__.'/../pages/contact.php';
        }
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData(){
    $user_json_content = file_get_contents(__DIR__.'/../data/user.json');
    return json_decode($user_json_content, true);
}
