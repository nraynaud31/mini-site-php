<main> <!-- <main> => différencier du header et footer -->
    <?php
    include'../parts/menu.php';
    ?>
    <h1><b>Formulaire de contact</b></h1>

    <form method="post" action="/../mini-site-php/pages/save.php" class="contact">
        <p>
            <label for="lastname">Votre nom: </label><br>
            <input type="text" name="lastname" required>
        </p>
        <p>
            <label for="firstname">Votre prénom: </label>
            <input type="text" name="firstname" required>
        </p>
        <p>
            <label for="email">Votre adresse mail: </label>
            <input type="email" name="email" required>
        </p>
        <p>
            <label for="message">Message: </label>
            <textarea name="message"></textarea>
        </p>
        <p>
            <input type="submit" value="Envoyer" name="send_content_form">
        </p>
    </form>
</main>
